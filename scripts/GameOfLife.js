/*
Conway�s Game of Life is a simple simulation that creates artificial �life� on a grid, each cell of which is either live or not. 
Each generation (turn), the following rules are applied:

 Any live cell with fewer than two or more than three live neighbors dies.

 Any live cell with two or three live neighbors lives on to the next generation.

Any dead cell with exactly three live neighbors becomes a live cell.

A neighbor is defined as any adjacent cell, including diagonally adjacent ones.

Note that these rules are applied to the whole grid at once, not one square at a time. That means the counting of
 neighbors is based on the situation at the start of the generation, and changes happening to neighbor cells during this 
generation should not influence the new state of a given cell.

Implement this game using whichever data structure you find appropriate. Use Math.random to populate the grid with a random 
pattern initially. Display it as a grid of checkbox fields, with a button next to it to advance to the next generation. 
When the user checks or unchecks the checkboxes, their changes should be included when computing the next generation.
 _________________________________________________________________________________________*/

function Critter(posRow, posCol, live) {

  this.posRow = posRow;
  this.posCol = posCol;
  this.live = live;
}

function Grid(width, height) {
  this.width = width;
  this.height = height;
  this.world = document.querySelector("#grid");
  this.actors = [];
  this.grid = [];

  for (var row = 0; row < this.height; row++) {

    for (var col = 0; col < this.width; col++) {

      var cell = document.createElement("input");
      cell.setAttribute("type", "checkbox");

      cell.checked = Math.random() > 0.5 ? true : false;
      cell.addEventListener("change", (event) => {

        this.actors[this.grid.indexOf(event.target)].live = event.target.checked;
      });
      this.world.appendChild(cell);
      this.grid.push(cell);
      this.actors.push(new Critter(row, col, cell.checked));

    }
    this.world.appendChild(document.createElement("br"));
  }
}

Grid.prototype.getCell = function (row, col) {

  return this.grid[col + (this.width * row)];
}

Critter.prototype.look = function (grid) {

  var neighbors = 0;

  for (var watchRow = this.posRow - 1; watchRow <= this.posRow + 1; watchRow++) {

    for (var watchCol = this.posCol - 1; watchCol <= this.posCol + 1; watchCol++) {

      var cell = grid.getCell(watchRow, watchCol)

      if ((watchRow >= 0 && watchCol >= 0) && (watchRow < grid.height && watchCol < grid.width)) {

        var cell = grid.getCell(watchRow, watchCol)
        if (cell.checked && cell != grid.getCell(this.posRow, this.posCol))
          neighbors++;
      }
    }
  }
  return neighbors;
}


Critter.prototype.act = function (grid) {

  var neighbors = this.look(grid);

  if (this.live) {

    if (neighbors < 2 || neighbors > 3) { this.live = false; }
  }
  else {

    if (neighbors == 3) { this.live = true; }
  }
}

Grid.prototype.turn = function () {

  this.actors.forEach(actor => {

    actor.act(this);
  });

  this.actors.forEach(actor => {

    this.getCell(actor.posRow, actor.posCol).checked = actor.live;
  });
}

var univers = new Grid(20, 20);

$("#next").click(function () { univers.turn(univers); });



