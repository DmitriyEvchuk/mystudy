import http from "http";
import fs from "fs";
import ReactDOMServer from 'react-dom/server';
import csshook from 'css-modules-require-hook/preset'
import Guests from "./guestsRename.jsx";
import guests from './data.js';

http.createServer(function (request, response) {

var path = require("url").parse(request.url).pathname;
  routing(path, response);
}).listen(8000);

function routing(path, response) {
console.log(path);
  if (path == "/") {

    response.writeHead(200, { "Content-Type": "text/html" });
   
   fs.readFile("../guestsRename.html", function(err, data) {
   
   if(err){console.log(err);}

  var str=data.toString().replace("{{{markup}}}", ReactDOMServer.renderToString(Guests));

   response.end(str);
});
  }

  if (path == "/scripts/guestsRename.js") {

    response.writeHead(200, { "Content-Type": "text/javascript" });
    fs.createReadStream('../scripts/guestsRename.js').pipe(response);
  }

  if (path == "/CSS/ForTable.css") {

    response.writeHead(200, { "Content-Type": "text/css" });
    fs.createReadStream('../CSS/ForTable.css').pipe(response);
  }
}

