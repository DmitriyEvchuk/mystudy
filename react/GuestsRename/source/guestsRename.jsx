import React from "react";
import ReactDOM from 'react-dom';
import '../CSS/ForTable.css';
import guests from './data.js';


class Guest extends React.Component {
  render() {
    return <tr  id={this.props.ID} onClick={this.props.onClick}>
      <td >{this.props.name}</td>
      <td  >{this.props.surName}</td>
    </tr>;
  }
}

class GuestsTable extends React.Component {
  render() {

    const listGueasts = this.props.guests.map(function (curGuest) {

      return <Guest onClick={this.props.onClick} key={curGuest.surname}
        ID={curGuest.surname} name={curGuest.name} surName={curGuest.surname} />
    }, this)

    return (
      <table >
        <thead>
          <tr>
            <th>Name</th>
            <th>Surname</th>
          </tr>
        </thead>
        <tbody>{listGueasts}</tbody>
      </table>);

  }
}

class InputBar extends React.Component {
  render() {
    return (
      <form onSubmit={this.props.onSubmit}>
        <input  onChange={this.props.onChange} value={this.props.value} type="text"  />
        <p>
          <input  type="submit" value="change" />
        </p>
      </form>
    );
  }
}

class Guests extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      guests: this.props.guests,
      value: ""
    };
    this.key;
    this.tableClick = this.tableClick.bind(this);
    this.inputChange = this.inputChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }

  onSubmit(e) {

    e.preventDefault();

    const guests = this.state.guests.map(function (guest) {

      if (guest.surname == this.key) {

        const val = this.state.value.split(" ");
        guest.name = val[0];
        guest.surname = val[1];

      }
      return guest;
    }, this)

    this.setState({ guests: guests, value: "" });
  }

  tableClick(e) {

    const guest = find(this.state.guests, e.target.parentNode.id);
    this.key = guest[0].surname;
    this.setState({ value: guest[0].name + " " + guest[0].surname });
  }

  inputChange(e) { this.setState({ value: e.target.value }); }

  render() {

    return (
      <div>
        <GuestsTable onClick={this.tableClick} guests={this.props.guests}/>
        <InputBar  onSubmit={this.onSubmit} onChange={this.inputChange} value={this.state.value}/>
      </div>
    )
  }
}

function find(guests, key) {

  return guests.filter(function (guest) {
    if (guest.surname == key) {
      return guest;
    }
  })
}


module.exports = <Guests guests={guests}/>;

