/*
Extend a text field so that when the user types, a list of suggested values is shown below the field. 
You have an array of possible values available and should show those that start with the text that was typed.
 When a suggestion is clicked, replace the text field�s current value with it.

*/

var terms = [];

for (var name in window) { terms.push(name); }


var listArea = document.querySelector("#suggestions");
var field = document.querySelector("#field");

field.addEventListener("input", function () {

  if (listArea.firstChild) { listArea.removeChild(listArea.firstChild); }

  var suited = terms.filter(function name(term) {

    return term.indexOf(field.value) == 0;
  });

  addList(suited);
});


function addList(suited) {

  var list = document.createElement("div");

  listArea.appendChild(list);

  suited.forEach(function (term) {

    var elemList = document.createElement("div");
    elemList.innerHTML = term;

    elemList.addEventListener("click", function () {

      field.value = term;
      listArea.removeChild(list);

    });

    list.appendChild(elemList);

  });

}
























