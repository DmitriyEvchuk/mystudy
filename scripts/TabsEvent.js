/*
Tabs

A tabbed interface is a common design pattern. It allows you to select an interface panel 
by choosing from a number of tabs �sticking out� above an element.

In this exercise you�ll implement a simple tabbed interface. Write a function, asTabs, 
that takes a DOM node and creates 

a tabbed interface showing the child elements of that node. It should insert a list of <button> 
elements at the top of the node, one for each child element, containing text retrieved from the 
data-tabname attribute of the child. All but one of the original 
children should be hidden (given a display style of none), and the currently visible node can be selected 
by clicking the buttons.

When it works, extend it to also style the currently active button differently.
 _____________________________________________________________________________*/

function tabsAreaCSS(tabsArea) {

    tabsArea.style.width = 545 + "px";
    tabsArea.style.height = 229 + "px";
    tabsArea.style.border = "1px solid rgb(186,186,186)";
    tabsArea.style.background = "rgb(222,222,222)";
    tabsArea.style.borderRadius = 4 + "px";

    return tabsArea;
}

function tabsAreaClick(tabsArea) {

    tabsArea.style.width = 100 + "%";
    tabsArea.style.height = 100 + "%";

    return tabsArea;
}

function labelsCSSDefault(label) {

    label.style.backgroundColor = "rgb(237,237,237)";
    label.style.font = "18px/20px Verdana";
    label.style.color = "rgb(115,116,115)";

    return label;
}

function labelsCSS(labels) {

    labels.forEach(function (label) {

        label.style.borderStyle = "solid solid none solid ";
        label.style.borderWidth = 1 + "px";
        label.style.borderColor = "rgb(186, 186, 186)";
        label.style.borderTopLeftRadius = 8 + "px";
        label.style.borderTopRightRadius = 8 + "px";
        label.style.textAlign = "center";
        labelsCSSDefault(label);
    });

    return labels;
}

function contentCss(content) {

    content.style.border = "1px solid rgb(212,210,211)";
    content.style.textAlign = "justify";
    content.style.background = "white";
    content.style.width = 97 + "%";
    content.style.height = 86 + "%";
    content.style.overflowY = "auto";
    content.style.borderRadius = 3 + "px";
    content.style.position = "relative";
    content.style.top = -1.6 + "px";

    return content;
}

function tabsStoreCssDefault(tabsStore) {

    tabsStore.style.width = 100 + "%";
    tabsStore.style.paddingRight = 40 + "%";
    tabsStore.style.tableLayout = "fixed";

    return tabsStore;
}

function tabsStoreClikc(tabsArea, lastEvent) {

    tabsArea.style.width = 545 + "px";
    tabsArea.style.height = 229 + "px";
    document.querySelector("#tabsStore").style.paddingRight = 40 + "%";

    var content = document.querySelector("#content")
    content.style.height = 86 + "%";
    content.textContent = null;

    labelsCSSDefault(lastEvent.target);
}

function contentClick(content) {

    content.textContent =
        document.querySelector("[data-tabname=" + event.target.textContent + "]").textContent;

    content.style.height = 94 + "%";

    return content;
}

function labelCSSMouseOver(event) {
    event.target.style.background = "rgb(222,222,222)";
}

function decorateTabsArea(tabsArea) {

    tabsAreaCSS(tabsArea);

    labelsCSS(document.querySelectorAll("#label"));

    contentCss(document.querySelector("#content"));

    tabsStoreCssDefault(document.querySelector("#tabsStore"));
}

function clickEvent(event, lastEvent) {

    if (lastEvent) { labelsCSSDefault(lastEvent.target) }

    tabsAreaClick(document.querySelector("#tabsArea"));
    contentClick(document.querySelector("#content"));

    event.target.style.background = "rgb(28,45,196)";
    event.target.style.color = "white";
}

function createTabsShell(node) {

    var tabsArea = document.createElement("div");
    tabsArea.id = "tabsArea";

    var tabs = document.createElement("table");
    tabs.id = "tabsStore";

    var labels = document.createElement("tr");
    labels.id = "labels";

    var label, lastEvent;

    tabs.addEventListener("click", function (event) {

        tabsStoreClikc(document.querySelector("#tabsArea"), lastEvent);

    });

    node.childNodes.forEach(function (child) {

        if (child.nodeType == document.ELEMENT_NODE) {

            label = document.createElement("td");

            label.addEventListener("mouseover", function (event) {

                if (lastEvent) {
                    if (lastEvent.target != event.target) {

                        labelCSSMouseOver(event);
                    }
                } else {
                    labelCSSMouseOver(event);
                }
            });

            label.addEventListener("mouseout", function (event) {

                if (lastEvent) {
                    if (lastEvent.target != event.target) {

                        labelsCSSDefault(event.target);
                    }
                } else {
                    labelsCSSDefault(event.target);
                }
            }
            );

            label.addEventListener("click", function (event) {

                tabs.style.paddingRight = 73.7 + "%";

                event.preventDefault();
                clickEvent(event, lastEvent);
                lastEvent = event;
                event.stopPropagation();

            });

            label.addEventListener("mousedown", function (event) {
                event.preventDefault(); //cancell allocation
            });

            label.textContent = child.getAttribute("data-tabname");
            label.id = "label";
            labels.appendChild(label);
        }
    });

    tabs.appendChild(labels);
    tabsArea.appendChild(tabs);

    var tabsContent = document.createElement("div");
    tabsContent.id = "content";
    tabsArea.appendChild(tabsContent);
    node.insertBefore(tabsArea, node.firstChild);

    return tabsArea;

}


function asTabs(node) {

    decorateTabsArea(createTabsShell(node));
}