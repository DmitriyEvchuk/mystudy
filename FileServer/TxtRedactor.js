
/*
A public space on the web

Since the file server serves up any kind of file and even includes the right Content-Type header, 
you can use it to serve a website. Since it allows everybody to delete and replace files, it would 
be an interesting kind of website: one that can be modified, vandalized, and destroyed by everybody who takes the 
time to create the right HTTP request. Still, it would be a website.

Write a basic HTML page that includes a simple JavaScript file. Put the files in a directory served by the 
file server and open them in your browser.

Next, as an advanced exercise or even a weekend project, combine all the knowledge you gained from this book 
to build a more user-friendly interface for modifying the website from inside the website.

Use an HTML form (Chapter 18) to edit the content of the files that make up the website, allowing the user to 
update them on the server by using HTTP requests as described in Chapter 17.

Start by making only a single file editable. Then make it so that the user can select which file to edit. 
Use the fact that our file server returns lists of files when reading a directory.

Don�t work directly in the code on the file server, since if you make a mistake you are likely to damage the files there. 
Instead, keep your work outside of the publicly accessible directory and copy it there when testing.

If your computer is directly connected to the Internet, without a firewall, router, or other interfering device in 
between, you might be able to invite a friend to use your website. To check, go to whatismyip.com, copy the 
IP address it gives you into the address bar of your browser, and add :8000 after it to select the right port. 
If that brings you to your site, it is online for everybody to see. */

var path;


function delFilesListContent() {

    var content = document.querySelector("#content");
    if (content) { content.remove(); }

    var filesList = document.querySelector("#files");
    if (filesList) { filesList.remove(); }
}


document.querySelector("#open").addEventListener("click", function () {

    delFilesListContent();

    loadFile();

});


function loadFile() {

    path = document.querySelector("#path").value;

    if (!path.length) { path = "."; }

    var req = new XMLHttpRequest();
    req.open("GET", path, true);
    req.addEventListener("load", function () {

        if (req.status == 202) {

            var filesList = document.createElement("select");
            document.body.appendChild(filesList);
            filesList.id = "files";

            ("Select file\n" + req.responseText).split("\n").forEach(function name(fileName) {

                var file = document.createElement("option");
                file.innerText = fileName;
                filesList.appendChild(file);

            });

            filesList.addEventListener("change", function () {

                document.querySelector("#path").value += "/" + filesList.value;
                filesList.remove();
                loadFile();

            });
            var content = document.querySelector("#content");
            if (content) { content.remove(); }

        } else {

            var content = document.createElement("textarea");
            content.id = "content";
            document.body.appendChild(content);

            content.value = req.responseText;
            content.cols = 160;
            content.rows = 40;
        }

    });

    req.addEventListener("error", function () { alert("Network error"); });
    req.send(null);
}


function PUTRequest(path, body = null) {
    var req = new XMLHttpRequest();

    req.open("PUT", path, true);
    req.addEventListener("load", function () {

        if (req.status < 400) { alert("Done: save in " + path + " " + req.status); }
        else
        { alert(req.responseText); }
    })

    req.addEventListener("error", function () { alert("Network error"); });

    req.send(body);
}


document.querySelector("#save").addEventListener("click", function () {

    if (document.querySelector("#content")) {

        PUTRequest(path, document.querySelector("#content").value)

    } else { alert("выбирите файл"); }
});


document.querySelector("#action").addEventListener("change", function (event) {

    var act = event.target.value;

    if (act) { action[act](); }
    event.target.value = "";
});


var action = {};

action.createFile = function () {

    path = document.querySelector("#path").value;

    if (path) {
        var req = new XMLHttpRequest();
        req.open("GET", path, true);
        req.addEventListener("load", function () {

            var isFile;
            if (req.status == 200) { isFile = confirm("Файл уже существует перезаписать его?"); }

            if ((req.status != 200) || isFile) { PUTRequest(path); }

            delFilesListContent();
            loadFile();
        });

        req.send(null);

    } else { alert("Введите путь"); }
}


function DELETERequest(path, callback, header) {


    var req = new XMLHttpRequest();
    req.open("DELETE", path, true);
    if (header) { req.setRequestHeader(header.name, header.value); }
    req.addEventListener("load", function () {
        callback(req);

    });

    req.addEventListener("error", function () { alert("Network error" + req.statusText); });
    req.send(null);
}


action.delFile = function () {

    path = document.querySelector("#path").value;

    if (path) {

        DELETERequest(path, (req) => {
            if (req.status > 204) { alert(req.responseText); }

            delFilesListContent();

        }, { name: "act", value: "DELETE" });

        document.querySelector("#path").value = path.replace(/(\/[^/]+)$/, "");

        loadFile();

    } else { alert("Введите путь"); }
}


action.createFolder = function () {

    path = document.querySelector("#path").value;

    if (path) {

        DELETERequest(path, (req) => {
            if (req.status == 204) {

                delFilesListContent();
            } else {

                alert(req.responseText);
                delFilesListContent();
            }
        }, { name: "act", value: "CREATE" });

        loadFile();

    } else { alert("Введите путь"); }

}


action.delFolder = function () {

    action.delFile();

};


document.querySelector("#path").addEventListener("keydown", function (event) {

    if (event.keyCode == 220) {

        event.preventDefault();
        event.target.value += "/";

    }
});