/*
Flood fill

This is a more advanced exercise than the preceding two, and it will require you to design a nontrivial solution to a tricky 
problem. Make sure you have plenty of time and patience before starting to work on this exercise, and do not get discouraged 
by initial failures.

A flood fill tool colors the pixel under the mouse and the surrounding pixels of the same color. For the purpose of this 
exercise, we will consider such a group to include all pixels that can be reached from our starting pixel by moving in 
single-pixel horizontal and vertical steps (not diagonal), without ever touching a pixel that has a color different 
from the starting pixel.

The following image illustrates the set of pixels colored when the flood fill tool is used at the marked pixel:

Flood fill example
The flood fill does not leak through diagonal gaps and does not touch pixels that are not reachable, even if they 
have the same color as the target pixel.

You will once again need getImageData to find out the color for each pixel. It is probably a good idea to fetch the 
whole image in one go and then pick out pixel data from the resulting array. The pixels are organized in this 
array in a similar way to the grid elements in Chapter 7, one row at a time, except that each pixel is represented by 
four values. The first value for the pixel at (x,y) is at position (x + y ? width) ? 4.

Do include the fourth (alpha) value this time since we want to be able to tell the difference between empty and black pixels.

Finding all adjacent pixels with the same color requires you to �walk� over the pixel surface, one pixel up, down, left,
or right, as long as new same-colored pixels can be found. But you won�t find all pixels in a group on the first walk. Rather,
you have to do something similar to the backtracking done by the regular expression matcher, described in Chapter 9.
Whenever more than one possible direction to proceed is seen, you must store all the directions you do not take immediately 
and look at them later, when you finish your 
current walk.

In a normal-sized picture, there are a lot of pixels. Thus, you must take care to do the minimal amount of work 
required or your program will take a very long time to run. For example, every walk must ignore pixels seen by 
previous walks so that it does not redo work that has already been done.

I recommend calling fillRect for individual pixels when a pixel that should be colored is found, and keeping some 
data structure that tells you about all the pixels that have already been looked at.

 */

//MY CODE BELOWE WITH COMMENT MY CODE
function elt(name, attributes) {
    var node = document.createElement(name);

    if (attributes) {
        for (var attr in attributes)
            if (attributes.hasOwnProperty(attr)) {
                node.setAttribute(attr, attributes[attr]);
            }
    }
    for (var i = 2; i < arguments.length; i++) {
        var child = arguments[i];
        if (typeof child == "string")
            child = document.createTextNode(child);
        node.appendChild(child);
    }
    return node;
}

var controls = Object.create(null);

function createPaint(parent) {
    var canvas = elt("canvas", { width: 500, height: 300 });

    var cx = canvas.getContext("2d");
    var toolbar = elt("div", { class: "toolbar" });

    for (var name in controls) {
        toolbar.appendChild(controls[name](cx));
    }

    var panel = elt("div", { class: "picturepanel" }, canvas);

    parent.appendChild(elt("div", null, panel, toolbar));


}

var tools = Object.create(null);

controls.tool = function (cx) {
    var select = elt("select");
    for (var name in tools)
        select.appendChild(elt("option", null, name));

    cx.canvas.addEventListener("mousedown", function (event) {
        if (event.which == 1) {
            tools[select.value](event, cx);
            event.preventDefault();
        }
    });

    return elt("span", null, "Tool: ", select);
};

controls.color = function (cx) {
    var input = elt("input", { type: "color" });
    input.addEventListener("change", function () {
        cx.fillStyle = input.value;
        cx.strokeStyle = input.value;
    });
    return elt("span", null, "Color: ", input);
};

function relativePos(event, element) {
    var rect = element.getBoundingClientRect();
    return {
        x: Math.floor(event.clientX - rect.left),
        y: Math.floor(event.clientY - rect.top)
    };
}


function trackDrag(onMove, onEnd) {
    function end(event) {
        removeEventListener("mousemove", onMove);
        removeEventListener("mouseup", end);
        if (onEnd)
            onEnd(event);
    }
    addEventListener("mousemove", onMove);
    addEventListener("mouseup", end);
}


tools.Line = function (event, cx, onEnd) {
    cx.lineCap = "round";

    var pos = relativePos(event, cx.canvas);
    trackDrag(function (event) {

        cx.beginPath();
        cx.moveTo(pos.x, pos.y);
        pos = relativePos(event, cx.canvas);
        cx.lineTo(pos.x, pos.y);
        cx.stroke();
    }, onEnd);
};

//MY CODE !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
function colorEqual(baseColor, nextColor) {

    for (colorVal in baseColor) {

        if (baseColor[colorVal] != nextColor[colorVal])
            return false;
    }
    return true;
}

//THIS SECOND FAST VERSION,BELOW IS FUNCTION 'Flood fill' FIRST VERSION
tools["Flood fill fast"] = function (event, cx) {

    var pixelGrid = cx.getImageData(0, 0, cx.canvas.width, cx.canvas.height).data;

    function getPixelCol(x, y) {

        var firstVal = (x + y * cx.canvas.width) * 4;
        return pixelGrid.slice(firstVal, firstVal + 4);
    }

    function setPixelCol(x, y) {

        var firstVal = (x + y * cx.canvas.width) * 4;

        for (var index = firstVal, ind = 0; index < firstVal + 4; index++ , ind++) {
            // set in grid fake color,dosen't metter what is this color,
            //the main thing that was different from base color
            pixelGrid[index] = baseColor[ind] != 255 ? baseColor[ind] + 1 : baseColor[ind] - 1;
        }
    }

    function fill(x, y) {

        var pixels = [];

        pixels.push({ x: x, y: y });

        while (pixels.length) {

            var pixel = pixels.shift();

            if (pixel.x < cx.canvas.width && pixel.y < cx.canvas.height && pixel.x >= 0 && pixel.y >= 0) {

                cx.fillRect(pixel.x, pixel.y, 1, 1);

                if (colorEqual(baseColor, getPixelCol(pixel.x + 1, pixel.y))) {

                    pixels.push({ x: pixel.x + 1, y: pixel.y });
                    setPixelCol(pixel.x + 1, pixel.y);
                }

                if (colorEqual(baseColor, getPixelCol(pixel.x, pixel.y + 1))) {

                    pixels.push({ x: pixel.x, y: pixel.y + 1 });
                    setPixelCol(pixel.x, pixel.y + 1);
                }

                if (colorEqual(baseColor, getPixelCol(pixel.x - 1, pixel.y))) {

                    pixels.push({ x: pixel.x - 1, y: pixel.y });
                    setPixelCol(pixel.x - 1, pixel.y);
                }

                if (colorEqual(baseColor, getPixelCol(pixel.x, pixel.y - 1))) {

                    pixels.push({ x: pixel.x, y: pixel.y - 1 });
                    setPixelCol(pixel.x, pixel.y - 1);
                }
            }
        }
    }

    var pos = relativePos(event, cx.canvas);
    var baseColor = getPixelCol(pos.x, pos.y);

    fill(pos.x, pos.y);
};

//EASY WAY,BUT SLOWLY - FIRST VERSION  !!!!!!
tools["Flood fill"] = function (event, cx) {

    function pixelAt(cx, x, y) {

        var data = cx.getImageData(x, y, 1, 1);
        return { R: data.data[0], G: data.data[1], B: data.data[2], alpha: data.data[3] };
    }

    function fill(x, y) {

        cx.fillRect(x, y, 1, 1);

        var nextColor = pixelAt(cx, x, y + 1);
        if (y < cx.canvas.height && colorEqual(baseColor, nextColor)) { fill(x, y + 1); }

        nextColor = pixelAt(cx, x, y - 1);
        if (y > 0 && colorEqual(baseColor, nextColor)) { fill(x, y - 1); }

        nextColor = pixelAt(cx, x + 1, y);
        if (x < cx.canvas.width && colorEqual(baseColor, nextColor)) { fill(x + 1, y); }

        nextColor = pixelAt(cx, x - 1, y);
        if (x > 0 && colorEqual(baseColor, nextColor)) { fill(x - 1, y); }
    }

    var pos = relativePos(event, cx.canvas);
    var baseColor = pixelAt(cx, pos.x, pos.y);
    fill(pos.x, pos.y);
};

tools.Erase = function (event, cx) {
    cx.globalCompositeOperation = "destination-out";
    tools.Line(event, cx, function () {
        cx.globalCompositeOperation = "source-over";
    });
};

tools.Spray = function (event, cx) {
    var radius = cx.lineWidth / 2;
    var area = radius * radius * Math.PI;
    var dotsPerTick = Math.ceil(area / 30);

    var currentPos = relativePos(event, cx.canvas);
    var spray = setInterval(function () {
        for (var i = 0; i < dotsPerTick; i++) {
            var offset = randomPointInRadius(radius);

            cx.beginPath();
            cx.arc(currentPos.x + offset.x, currentPos.y + offset.y, 0.5, 0, 7);
            cx.fill();

        }
    }, 25);
    trackDrag(function (event) {
        currentPos = relativePos(event, cx.canvas);
    }, function () {
        clearInterval(spray);
    });
};


tools.Text = function (event, cx) {
    var text = prompt("Text:", "");
    if (text) {
        var pos = relativePos(event, cx.canvas);
        cx.font = Math.max(7, cx.lineWidth) + "px sans-serif";
        cx.fillText(text, pos.x, pos.y);
    }
};


controls.brushSize = function (cx) {
    var select = elt("select");
    var sizes = [1, 2, 3, 5, 8, 12, 25, 35, 50, 75, 100];
    sizes.forEach(function (size) {
        select.appendChild(elt("option", { value: size },
            size + " pixels"));
    });
    select.addEventListener("change", function () {
        cx.lineWidth = select.value;
    });
    return elt("span", null, "Brush size: ", select);
};

controls.save = function (cx) {
    var link = elt("a", { href: "/" }, "Save");
    function update() {
        try {
            link.href = cx.canvas.toDataURL();
        } catch (e) {
            if (e instanceof SecurityError)
                link.href = "javascript:alert(" +
                    JSON.stringify("Can't save: " + e.toString()) + ")";
            else
                throw e;
        }
    }
    link.addEventListener("mouseover", update);
    link.addEventListener("focus", update);
    return link;
};

function loadImageURL(cx, url) {
    var image = document.createElement("img");
    image.addEventListener("load", function () {
        var color = cx.fillStyle, size = cx.lineWidth;
        cx.canvas.width = image.width;
        cx.canvas.height = image.height;
        cx.drawImage(image, 0, 0);
        cx.fillStyle = color;
        cx.strokeStyle = color;
        cx.lineWidth = size;
    });
    image.src = url;
}

controls.openFile = function (cx) {
    var input = elt("input", { type: "file" });
    input.addEventListener("change", function () {
        if (input.files.length == 0) return;
        var reader = new FileReader();
        reader.addEventListener("load", function () {
            loadImageURL(cx, reader.result);
        });
        reader.readAsDataURL(input.files[0]);
    });
    return elt("div", null, "Open file: ", input);
};

controls.openURL = function (cx) {
    var input = elt("input", { type: "text" });
    var form = elt("form", null,
        "Open URL: ", input,
        elt("button", { type: "submit" }, "load"));
    form.addEventListener("submit", function (event) {
        event.preventDefault();

        loadImageURL(cx, form.querySelector("input").value);
    });
    return form;
};

function randomPointInRadius(radius) {
    for (; ;) {
        var x = Math.random() * 2 - 1;
        var y = Math.random() * 2 - 1;
        if (x * x + y * y <= 1)
            return { x: x * radius, y: y * radius };
    }
}

createPaint(document.body);

